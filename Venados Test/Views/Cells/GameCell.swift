//
//  GameCell.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 7/30/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import UIKit
import Kingfisher

protocol gameCellDelegate: AnyObject {
    func btnCalendarTapped(cell: GameCell)
}

class GameCell: UITableViewCell {

    public static let reuseIdentifier = "GameCell"
    
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var lblNumberDay: UILabel!
    @IBOutlet weak var lblNameDay: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblOpponentLabel: UILabel!
    @IBOutlet weak var imgOpponent: UIImageView!
    
    weak var delegate: gameCellDelegate?
    
    @IBAction func btnCalendarTapped(_ sender: AnyObject) {
        delegate?.btnCalendarTapped(cell: self)
    }
    
    public func configureWith(_ game: Game) {
        
        var calendar = Calendar.current
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "es") as Locale?
        dateFormatter.timeZone = calendar.timeZone
        dateFormatter.dateFormat = "EE"
        let dateComponents : Set<Calendar.Component> = [ .day ]
        var components: DateComponents? = calendar.dateComponents(dateComponents, from: game.datetime)
        let day: Int = (components?.day)!
        
        let dayInWeek = dateFormatter.string(from: game.datetime)
        
        lblScore.text = "\(game.home_score) - \(game.away_score)"
        lblOpponentLabel.text = game.opponent
        lblNumberDay.text = "\(day)"
        lblNameDay.text = dayInWeek
        imgOpponent.kf.setImage(with: URL(string: game.opponent_image), options: [.transition(.fade(0.3))])
        
    }
    

}
