//
//  PlayerCell.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 7/31/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import UIKit
import Kingfisher

class PlayerCell: UICollectionViewCell {
    
    public static let reuseIdentifier = "PlayerCell"
    
    //IBOutlets
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    
    public func configureWith(_ player: Player) {
        
        imgPlayer.layer.masksToBounds = false
        imgPlayer.layer.cornerRadius = imgPlayer.frame.height/2
        imgPlayer.clipsToBounds = true
        
        imgPlayer.kf.setImage(with: URL(string: player.image))
        lblPosition.text = player.position
        lblPlayerName.text = "\(player.name) \(player.first_surname)"
        
    }
    
}
