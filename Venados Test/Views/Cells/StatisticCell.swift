//
//  StatisticCellTableViewCell.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 30/07/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import UIKit
import Kingfisher

class StatisticCell: UITableViewCell {

    public static let reuseIdentifier = "StatisticCell"
    
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var imgTeam: UIImageView!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblJJ: UILabel!
    @IBOutlet weak var lblDG: UILabel!
    @IBOutlet weak var lblPTS: UILabel!
    
    public func configureWith(_ statistic: Statistic) {
        
        lblPosition.text = "\(statistic.position)"
        imgTeam.kf.setImage(with: URL(string: statistic.image), options: [.transition(.fade(0.3))])
        lblTeamName.text = statistic.team
        lblJJ.text = "\(statistic.games)"
        lblDG.text = "\(statistic.score_diff)"
        lblPTS.text = "\(statistic.points)"
        
    }
    
}
