//
//  Date+ConvertToString.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 7/31/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import Foundation

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}
