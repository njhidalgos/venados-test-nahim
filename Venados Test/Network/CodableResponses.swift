//
//  CodableResponses.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 7/30/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import Foundation

struct GameResponse<T: Codable>: Codable {
    let data: GamesResults<T>
}

struct GamesResults<T: Codable>: Codable {
    let games: [T]
}

struct StatisticsResponse<T: Codable>: Codable {
    let data: StatisticssResults<T>
}

struct StatisticssResults<T: Codable>: Codable {
    let statistics: [T]
}

struct PlayersForwardsResults<T: Codable>: Codable {
    let data: TeamForwardsResults<T>
}

struct TeamForwardsResults<T: Codable>: Codable {
    let team: ForwardResults<T>
}

struct ForwardResults<T: Codable>: Codable {
    let forwards: [T]
}

struct PlayersCentersResults<T: Codable>: Codable {
    let data: TeamCenterResults<T>
}

struct TeamCenterResults<T: Codable>: Codable {
    let team: CenterResults<T>
}

struct CenterResults<T: Codable>: Codable {
    let centers: [T]
}

struct PlayersDefensesResults<T: Codable>: Codable {
    let data: TeamDefenseResults<T>
}

struct TeamDefenseResults<T: Codable>: Codable {
    let team: DefenseResults<T>
}

struct DefenseResults<T: Codable>: Codable {
    let defenses: [T]
}

struct PlayersGoalkeepersResults<T: Codable>: Codable {
    let data: TeamGoalkeepersResults<T>
}

struct TeamGoalkeepersResults<T: Codable>: Codable {
    let team: GoalkeeperResults<T>
}

struct GoalkeeperResults<T: Codable>: Codable {
    let goalkeepers: [T]
}
