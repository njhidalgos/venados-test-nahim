//
//  Venados.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 7/30/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import Foundation
import Moya

public enum Venados {
    
    case games
    case statistics
    case players
    
}

extension Venados: TargetType {
    
    public var baseURL: URL {
        return URL(string: "https://venados.dacodes.mx/api")!
    }
    
    public var path: String {
        switch self {
        case .games: return "/games"
        case .statistics: return "/statistics"
        case .players : return "/players"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .games,.statistics,.players: return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .games,.statistics,.players: return .requestPlain
        }
    }
    
    public var headers: [String : String]? {
        return ["Accept": "application/json"]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
    
}
