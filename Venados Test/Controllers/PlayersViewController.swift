//
//  PlayersViewController.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 30/07/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import UIKit
import Moya

class PlayersViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    let provider = MoyaProvider<Venados>()
    var players: [Player] = []
    
    @IBOutlet weak var playersCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.playersCollectionView.delegate = self
        
        provider.request(.players) { [weak self] result in
            
            guard case self = self else { return }
            
            switch result {
            case .success(let response):
                
                do {
                    
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    
                    self?.players.append(contentsOf: try response.map(PlayersForwardsResults<Player>.self, atKeyPath: nil, using: decoder, failsOnEmptyData: false).data.team.forwards)
                    self?.players.append(contentsOf: try response.map(PlayersCentersResults<Player>.self, atKeyPath: nil, using: decoder, failsOnEmptyData: false).data.team.centers)
                    self?.players.append(contentsOf: try response.map(PlayersDefensesResults<Player>.self, atKeyPath: nil, using: decoder, failsOnEmptyData: false).data.team.defenses)
                    self?.players.append(contentsOf: try response.map(PlayersGoalkeepersResults<Player>.self, atKeyPath: nil, using: decoder, failsOnEmptyData: false).data.team.goalkeepers)
                    
                    self?.playersCollectionView.reloadData()
                    
                } catch {
                    print(error)
                }
            case .failure:
                print(result.error)
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func presentMenu(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "sideMenu")
        self.present(controller, animated: true, completion: nil)
    }
    
    // MARK: - Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.players.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlayerCell.reuseIdentifier, for: indexPath) as? PlayerCell ?? PlayerCell()
        cell.configureWith(self.players[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let cell = collectionView.cellForItem(at: indexPath) else { return }
        
        performSegue(withIdentifier: "showPlayerDetailsSegue", sender: cell)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showPlayerDetailsSegue":
            
            if let cell = sender as? PlayerCell,
                let indexPath = self.playersCollectionView.indexPath(for: cell) {
                
                let vc = segue.destination as! PlayerDetailsViewController
                //Now simply set the title property of vc
                vc.player = players[indexPath.row]
            }
            
            
        default: return
        }
    }
    
}
