//
//  StatisticsViewController.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 30/07/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import UIKit
import Moya

class StatisticsViewController: UIViewController, UITableViewDataSource {

    let provider = MoyaProvider<Venados>()
    var statistics: [Statistic] = []
    
    @IBOutlet weak var statisticsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        provider.request(.statistics) { [weak self] result in
            
            guard case self = self else { return }
            
            switch result {
            case .success(let response):
                
                do {
                    
                    self?.statistics = try response.map(StatisticsResponse<Statistic>.self).data.statistics
                    self?.statisticsTableView.reloadData()
                    
                } catch {
                    print(error)
                }
            case .failure:
                print(result.error)
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func presentMenu(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "sideMenu")
        self.present(controller, animated: true, completion: nil)
    }
    
    // MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.statistics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: StatisticCell.reuseIdentifier, for: indexPath) as? StatisticCell ?? StatisticCell()
        
        cell.configureWith(self.statistics[indexPath.row])
        
        return cell
        
    }

}
