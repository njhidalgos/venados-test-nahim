//
//  MenuViewController.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 30/07/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import UIKit
import SideMenu

class MenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        SideMenuManager.default.menuFadeStatusBar = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func presentHome(_ sender: Any) {
        
        performSegue(withIdentifier: "unwindSegueToHomeVC", sender: self)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
