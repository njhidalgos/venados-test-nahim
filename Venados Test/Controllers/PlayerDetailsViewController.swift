//
//  PlayerDetailsViewController.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 7/31/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import UIKit
import Kingfisher

class PlayerDetailsViewController: UIViewController {

    var player: Player?
    
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var lblPlayerPosition: UILabel!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var lblPlaceOfBirth: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblLastTeam: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        if let player = player {
            setupView(playerInfo: player)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView(playerInfo: Player) {
        imgPlayer.layer.masksToBounds = false
        imgPlayer.layer.cornerRadius = imgPlayer.frame.height/2
        imgPlayer.clipsToBounds = true
        imgPlayer.kf.setImage(with: URL(string: playerInfo.image))
        lblPlayerName.text = "\(playerInfo.name) \(playerInfo.first_surname) \(playerInfo.second_surname)"
        lblPlayerPosition.text = playerInfo.position
        lblBirthday.text = playerInfo.birthday.toString(dateFormat: "dd/MM/yyyy")
        lblPlaceOfBirth.text = playerInfo.birth_place
        lblWeight.text = "\(playerInfo.weight) KG"
        lblHeight.text = "\(playerInfo.height) M"
        lblLastTeam.text = playerInfo.last_team
    }
    
    @IBAction func dismissModal(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
