//
//  GamesViewController.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 7/30/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import UIKit
import Moya
import EventKit

class GamesViewController: UIViewController, UITableViewDataSource,UITableViewDelegate, gameCellDelegate {
    
    func btnCalendarTapped(cell: GameCell) {
        
        let indexPath = self.gamesTableView.indexPath(for: cell)
        let selectedGame = self.filteredGamesByMonth[(indexPath?.section)!][(indexPath?.row)!]
        
        addEventToCalendar(game: selectedGame) { (completed, error) in
            
            if completed == true {
                
                let alert = UIAlertController(title: "!Bien!", message: "El partido se guardo en tu calendario", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cerrar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    @IBOutlet weak var gamesTableView: UITableView!
    @IBAction func unwindToHomeVC(segue:UIStoryboardSegue) { }
    
    let provider = MoyaProvider<Venados>()
    var games: [Game] = []
    var sectionTitles: [String] = []
    var filteredGamesByMonth: [[Game]] = []
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.gamesTableView.delegate = self
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            gamesTableView.refreshControl = refreshControl
        } else {
            gamesTableView.addSubview(refreshControl)
        }
        
        getGames()
        
        refreshControl.addTarget(self, action: #selector(refreshGamesData(_:)), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Obteniendo información de juegos ...", attributes: nil)
        
    }
    
    @IBAction func filterByCopaMX(_ sender: Any) {
        filterGamesBy(championship: "Copa MX")
    }
    
    @IBAction func filterByAscensoMX(_ sender: Any) {
        filterGamesBy(championship: "Ascenso MX")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getGames() {
        
        provider.request(.games) { [weak self] result in
            
            guard case self = self else { return }
            
            switch result {
            case .success(let response):
                
                do {
                    
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    
                    self?.games = try response.map(GameResponse<Game>.self, atKeyPath: nil, using: decoder, failsOnEmptyData: false).data.games
                    
                    self?.filterGamesBy(championship: "Copa MX")
                    
                } catch {
                    print(error)
                }
            case .failure:
                print(result.error)
            }
            
        }
        
    }
    
    @objc private func refreshGamesData(_ sender: Any) {
        // Fetch Games Data
        getGames()
    }
    
    func filterGamesBy(championship: String) {
        
        self.sectionTitles.removeAll()
        self.filteredGamesByMonth.removeAll()
        
        // Se obtienen los objetos filtrados por liga
        let filteredGames = (self.games.filter({ (game) -> Bool in
            game.league == championship
        }))
        
        var calendar = Calendar.current
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "es") as Locale?
        dateFormatter.timeZone = calendar.timeZone
        dateFormatter.dateFormat = "MMMM"
        
        let dateComponents : Set<Calendar.Component> = [ .year , .month ]
        var previousYear: Int = -1
        var previousMonth: Int = -1
        
        for game in filteredGames {
            
            // componentes from current game date
            var components: DateComponents? = calendar.dateComponents(dateComponents, from: game.datetime)
            let year: Int? = components?.year
            let month: Int? = components?.month
            
            if year != previousYear || month != previousMonth {
                
                let sectionHeading = dateFormatter.string(from: game.datetime)
                self.sectionTitles.append(sectionHeading)
                previousYear = year!
                previousMonth = month!
                
                // We get the games of the month
                let gamesOfTheCurrentMonth = filteredGames.filter {
                    return Calendar.current.component(.month, from: $0.datetime) == month
                }
                
                self.filteredGamesByMonth.append(gamesOfTheCurrentMonth)
                
            }
            
        }
        
        self.gamesTableView.reloadData()
        self.refreshControl.endRefreshing()
        
    }
    
    func addEventToCalendar(game: Game, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                
                let calendar = Calendar.current
                
                event.title = "Venados F.C. VS \(game.opponent)"
                event.startDate = game.datetime
                event.endDate = calendar.date(byAdding: .hour, value: 1, to: game.datetime)
                //event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 22
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionTitles[section]
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 18))
        let label = UILabel(frame: CGRect(x: 10, y: 2, width: tableView.frame.size.width, height: 18))
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = self.sectionTitles[section]
        view.addSubview(label)
        view.backgroundColor = UIColor.lightGray // Set your background color
        
        return view
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredGamesByMonth[section].count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GameCell.reuseIdentifier, for: indexPath) as? GameCell ?? GameCell()
        
        cell.delegate = self
        
        cell.configureWith(self.filteredGamesByMonth[indexPath.section][indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 100.0;
    }

}
