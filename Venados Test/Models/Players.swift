//
//  Players.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 7/31/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import Foundation

struct Player: Codable {
    
    let name: String
    let first_surname: String
    let second_surname: String
    let birthday: Date
    let birth_place: String
    let weight: Int
    let height: Double
    let position: String
    let number: Int
    let position_short: String
    let last_team: String
    let image: String

}
