//
//  Games.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 7/30/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import Foundation

struct Game: Codable {
    
    let local: Bool
    let opponent: String
    let opponent_image: String
    let datetime: Date
    let league: String
    let image: String
    let home_score: Int
    let away_score: Int
    
}

