//
//  Statistic.swift
//  Venados Test
//
//  Created by Nahim Hidalgo on 30/07/18.
//  Copyright © 2018 Nahim Hidalgo. All rights reserved.
//

import Foundation

struct Statistic: Codable {
    
    let position: Int
    let image: String
    let team: String
    let games: Int
    let score_diff: Int
    let points: Int
    
}
