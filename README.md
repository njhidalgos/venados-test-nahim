# Venados Test

This application allows you to know relevant information about the team Venados C.F.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- Xcode 9.4 or newer
- Some Version Control Software like tower or sourcetree

### Installing

- Download the source code from https://bitbucket.org/njhidalgos/venados-test-nahim/src/master/
- Inside the root folder open Venados Test.xcworkspace, this allow you to open the project
- Wait for Xcode for index the project 
- In case you cannot run on simulator try to run the command "pod install" on the root folder of the project
- For run on a real device you may need to change the Bundle Identifier and Team on the targets view preferences
- If everything is fine you can go to the menu "Product -> Run" or Command+R to run the project

End with an example of getting some data out of the system or using it for a little demo

## Built With

* [Cocoapods](https://github.com/CocoaPods/CocoaPods) - The Cocoa dependency manager
* [Moya](https://github.com/Moya/Moya) - Network abstraction layer written in Swift. 
* [SideMenu](https://github.com/jonkykong/SideMenu) - Simple side menu control for iOS
* [KingFisher](https://github.com/onevcat/Kingfisher) - A lightweight, pure-Swift library for downloading and caching images from the web.

## Authors

* **Nahim Hidalgo** - *iOS Dev*


